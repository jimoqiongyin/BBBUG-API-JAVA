package com.bbbug.coree.mapper;

import com.bbbug.coree.entity.Lrcinfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface LrcinfoMapper {
    @Select("select id from lrcinfo where id=#{id}")
    Lrcinfo queryId(Integer id);
    @Select("select * from lrcinfo where id=#{id}")
    Lrcinfo queryById(Integer id);
    @Insert("insert into lrcinfo (id,lrc) value (#{id},#{lrc})")
    Boolean insert(Integer id,String lrc);
}
