如果使用之前的版本（2021.8.20之前）的数据库 请添加数据库字段
````
ALTER TABLE roominfo ADD room_pushsongcd int;
ALTER TABLE roominfo ADD room_playone int;
ALTER TABLE roominfo ADD room_votepass int;
ALTER TABLE roominfo ADD room_addcount int;
ALTER TABLE roominfo ADD room_addsongcd int;
ALTER TABLE roominfo ADD room_pushdaycount int;
````